<?php 
/**
 * Theme Footer Section for our theme.
 * 
 * Displays all of the footer section and closing of the #main div.
 *
 * @package ThemeGrill
 * @subpackage Spacious
 * @since Spacious 1.0
 */
?>

		</div><!-- .inner-wrap -->
	</div><!-- #main -->	
	<?php do_action( 'spacious_before_footer' ); ?>
		<footer id="colophon" class="clearfix">	
			<?php get_sidebar( 'footer' ); ?>	
			<div class="footer-socket-wrapper clearfix">
				<div class="inner-wrap" style="margin: auto; max-width: inherit;">
					<div class="footer-socket-area" style=" text-align: center;"> <!-- .RichiLOL -->
						<div class="row" style="position: relative; display: inline-table">
							<div class="col-md-6 contact-logo">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home";><img src="<?php echo spacious_options( 'spacious_header_logo_image', '' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
								<h3 id="site-title">
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
								</h3>
							</div>
							<div class="col-md-6 contact"> <!-- .RichiLOL -->
								<span>
									<img src="<?php echo get_template_directory_uri() ?>/imgs/Entremes-01.png">
									Carrera 17 #93a- 02
								</span><br>
								<span>
									<img src="<?php echo get_template_directory_uri() ?>/imgs/Entremes-02.png"">
									57 1 4796210
								</span><br>
								<span>
									<img src="<?php echo get_template_directory_uri() ?>/imgs/Entremes-03.png">
									info@entremesvm.com
								</span>
							</div>
						</div>
						<div style="background-color: rgb(71, 178, 232); text-align: center; color: white;padding-bottom: 10px;">
							<br>
							Copyright ENTREMÉS MÁQUINAS DISPENSADORAS. All rights reserved.
						</div>

						<nav class="small-menu clearfix">
							<?php
								if ( has_nav_menu( 'footer' ) ) {									
										wp_nav_menu( array( 'theme_location' => 'footer',
																 'depth'           => -1
																 ) );
								}
							?>
		    			</nav>
					</div>
				</div>
			</div>			
		</footer>
		<a href="#masthead" id="scroll-up"></a>	
	</div><!-- #page -->
	<?php wp_footer(); ?>
</body>
</html>